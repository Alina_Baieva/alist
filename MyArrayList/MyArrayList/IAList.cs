﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyArrayList
{
    interface IAList
    {
        public void Clear(); // очистка коллекции
        public int Size(); // возвращает количество элементов в коллекции
        public int Get(int index); // возвращает элемент по индексу
        public bool Add(int value); //добавляет элемент в коллекцию и возвращает в случае успеха true
        public bool Add(int index, int value); // добавляет элемент в коллекцию по указаному индексу но не перезаписывая существующее значение и возвращает в случае успеха true
        public int Remove(int value); //удаление элемента по значению и возврат этого элемента
        public int RemoveByIndex(int index); // удаление элемента по индексу и возврат этого элемента
        public bool Contains(int value); //возвращает true если такое значение в коллекции есть
        public bool Set(int index, int value); // добавляет элемент в коллекцию по указаному индексу перезаписывая существующее значение если такого уже есть по указанному индексу и возвращает в случае успеха true
        public void Print(); // выводит в консоль массив в квадратных скобках и через запятую
        public int[] ToArray(); //приводит данные к массиву, в случае с AList ничего сложного, у нас и так массив
        public bool RemoveAll(int[] arr); // удаляет все элементы в коллекции которые есть в переданном масиве ar
    }
}
