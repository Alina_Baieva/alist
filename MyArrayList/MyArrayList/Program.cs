﻿using System;

namespace MyArrayList
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2};

            myArrayList myArray = new myArrayList( new int[] { 1, 2, 3, 4, 5, 6, 7, 1, 0, 23, 2 });
            myArrayList myArray1 = new myArrayList();
            myArrayList arrayList = new myArrayList();
            //Console.WriteLine(myArray.Get(44));
            myArray.RemoveAll(new int[] { 1, 5, 99, 0 });
            myArray.Print();
        }
    }
}
