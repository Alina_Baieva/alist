using NUnit.Framework;
using MyArrayList;
namespace UnitTest_MyArrayList
{
    public class Tests
    {
        myArrayList arrayList;
        int[] _array = { 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2 };
        int size = 15;

        [TestCase(new int[14]{ 1, 2, 3, 4, 5, 6, 7, 12, 23, 32, 23, 1, 0, 2 }, 14)]
        [TestCase(new int[1]{1}, 1)]
        public void CheckSizeOfArrayList(int[] array, int excpected)
        {
            arrayList = new myArrayList(array);
            var actual = arrayList.Size();
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 2)]
        [TestCase(0, 1)]
        [TestCase(13, 2)]
        [TestCase(14, 2)]
        public void CheckGetElementFromArrayList(int index, int excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Get(index);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, true)]
        [TestCase(0, true)]
        [TestCase(13, true)]
        public void CheckAddedToArrayList(int value, bool excpected)
        {
            arrayList = new myArrayList();
            var actual = arrayList.Add(value);
            Assert.AreEqual(excpected, actual);
        }
        [TestCase(-1, 6, false)]
        [TestCase(0, 7, true)]
        [TestCase(13, 9, true)]
        [TestCase(14, 9, false)]
        public void CheckChangeToArrayList(int index, int value, bool excpected)
        {
            arrayList = new myArrayList(_array);
            var actual = arrayList.Add(index, value);
            Assert.AreEqual(excpected, actual);
        }
        
       
    }
}